# Visual Creativity Informatics

## Project topic - Reaction Diffusion System
## Main Idea

I would love to create what is called a Reaction Diffusion System. The main idea is that this
algorithm looks and works like an organic structure that possesses a behavior similar to a large
range of chemicals. The name ' Reaction Diffusion ' means that there exists some kind of reaction
between the chemicals that causes them to spread onto a larger and larger surface. The structure's
appearance can vary based on multiple variables in the algorithm that are necessary to create this
reaction.

## Expected Outcomes
I have two main ideas on how could I work with the reaction diffusion system. (I don’t know for now
which one I’ll pick but I will propably stick to the second one) :
1. Users will be able to input an image into the program, and the Reaction Diffusion Algorithm will
create a new artwork based on the input image. This would be done by adding colors from the
original artwork, as well as the system working differently based on different input. For now, I
don't know how I would implement it, but that will be my challenge for next month. I don't think
it's that hard to implement. I also think I saw some people who have already implemented it, so
it shouldn't be that difficult. The results could look something like this:
2. Users will have a 'sandbox' project where they can experiment with the algorithm itself. There
would be many variables that the user could change in the artwork, as in the first outcome.
One example of how I imagine this is on the Karl Sims webpage. He created a web application
to experiment with these simulations, and I really liked it. At the same time, I don't want to
create something that already exists, so I would try to differ from his webpage. But, at the same
time, this simulation of chemicals fascinates me, so maybe I would like a plain experimental tool
to just look at various systems and how they react.
Karl Sims also created a little tutorial on how to make these simulations, which I followed for the
first steps in order to create something for the start. Some of the results that can be achieved
from his demo:

## Project Status

For now, I tried to create this system with the help of Nature of Code in p5.js through a video
tutorial, and it worked, but on a small canvas, and it was also really slow.

I converted this code from p5.js into Processing, but I found out that because of my old laptop the
program is a bit slow. I know this is caused mainly due to the performance of my laptop, but on
better performing devices it can be fast. I am trying to optimize the performance on my machine by
limiting the drawing of the canvas and I will also try to do some subsampling. I don’t know if it will
help much, but I don’t know how to work with things like OpenGL or multithreading, so I will just try
with the classic algorithmic optimization.

## Programming Language
Processing

