// file: grayscott.frag
// author: diewald

// NOTE: This code is not mine, as I didn't completely know how to work with shaders
// I've added in this shader after trying out the application logic, which was too slow to compute in 4K
// Original source: https://forum.processing.org/two/discussion/22385/reaction-diffusion-using-glsl-works-different-from-normal.html

#version 150

out vec4 fragColor;

uniform vec2 wh_rcp;
uniform sampler2D tex;

uniform float dA;
uniform float dB;
uniform float feed;
uniform float kill;
uniform float dt;

// Decodes one RGBA channel into two chemicals (A and B)
vec2 decode(vec4 dataf) {
  ivec4 datai = ivec4(dataf * 255.0); // intovy vektor
  float rg = (datai.r << 8 | datai.g) / 65535.0; // pracovanie s 16 bit hodnotami
  float ba = (datai.b << 8 | datai.a) / 65535.0;
  return vec2(rg, ba); // vracia to ako floatovy vektor
}

// Encodes two chemicals (A and B) into one RGBA channel 
vec4 encode(vec2 dataf) {
  ivec2 datai = ivec2(dataf * 65535.0);
  int r = (datai.r >> 8) & 0xFF;
  int g = (datai.r     ) & 0xFF;
  int b = (datai.g >> 8) & 0xFF;
  int a = (datai.g     ) & 0xFF;
  return vec4(r,g,b,a) / 255.0;
}

// Calculates Gray-Scott Reaction Diffusion
void main () {
  vec2 posn = gl_FragCoord.xy * wh_rcp;
  
  vec2 val = decode(texture(tex, posn));
  
  vec2 laplace = -val;
  laplace += decode(textureOffset(tex, posn, ivec2(-1, 0))) * + 0.20;
  laplace += decode(textureOffset(tex, posn, ivec2(+1, 0))) * + 0.20;
  laplace += decode(textureOffset(tex, posn, ivec2( 0,-1))) * + 0.20;
  laplace += decode(textureOffset(tex, posn, ivec2( 0,+1))) * + 0.20;
  laplace += decode(textureOffset(tex, posn, ivec2(-1,-1))) * + 0.05;
  laplace += decode(textureOffset(tex, posn, ivec2(+1,-1))) * + 0.05;
  laplace += decode(textureOffset(tex, posn, ivec2(-1,+1))) * + 0.05;
  laplace += decode(textureOffset(tex, posn, ivec2(+1,+1))) * + 0.05;
  
  float nA = val.r + (dA * laplace.r - val.r * val.g * val.g + (feed * (1.0 - val.r))) * dt;
  float nB = val.g + (dB * laplace.g + val.r * val.g * val.g - ((kill + feed) * val.g)) * dt;

  fragColor = encode(clamp(vec2(nA, nB), vec2(0), vec2(1)));
}