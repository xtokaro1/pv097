// author: diewald

// NOTE: This code partially not mine, as I didn't completely know how to work with shaders
// I've added in this shader after trying out the application logic, which was too slow to compute in 4K
// I've adjusted the main function and added calculateColor so that we can have a custom visual of the reaction diffusion
// Original source: https://forum.processing.org/two/discussion/22385/reaction-diffusion-using-glsl-works-different-from-normal.html

#version 150

out vec4 fragColor;

uniform vec2 wh_rcp;
uniform sampler2D tex;
uniform vec3 targetColor;
uniform vec3 secondaryColor;
uniform vec3 backgroundColor;

// Decodes one RGBA channel into two chemicals (A and B)
vec2 decode(vec4 dataf) {
  ivec4 datai = ivec4(dataf * 255.0);
  float rg = (datai.r << 8 | datai.g) / 65535.0;
  float ba = (datai.b << 8 | datai.a) / 65535.0;
  return vec2(rg, ba);
}

// Calculates the color for a given cell
vec3 calculateColor(float value) {
  float splitPart = 0.6;
  if (value < splitPart) {
    return mix(targetColor / 255.0, secondaryColor / 255.0, value / splitPart);
  }
  return mix(secondaryColor / 255.0, backgroundColor / 255.0, (value - splitPart) / (1 - splitPart));
}

// Adds color to every single cell of the Reaction Diffusion
void main () {
  vec2 compound = decode(texture(tex, gl_FragCoord.xy * wh_rcp));
  fragColor = vec4(calculateColor(compound.x - compound.y), 1);
}