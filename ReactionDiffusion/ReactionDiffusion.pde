import processing.opengl.*;
import java.*;
import com.krab.lazy.*;

/* Configuration values */

String title = "RDS - Reaction Diffusion Simulation";

/* Paths of the shaders*/
String computeShader = "grayscott.frag";
String renderShader = "render.frag";

/* Initial mode of the application */
String selectedMode = "solitons";

/* Gray-Scott values of the Reaction Diffusion */
float dA = 1.0f;
float dB = 0.5f;

float feed = 0.055f;
float kill = 0.062f;

float dT = 1.0f;

/* Drawing settings */
float brushSize = 500;

color backgroundColor = #FFFFFF;
color targetColor = #32EE90;
color secondaryColor = #0000FF;

/* Final canvas size */
int sourceWidth = 3840;
int sourceHeight = 2160;

/* Used computational globals */
LazyGui gui;

PShader shader_grayscott;
PShader shader_render;

PGraphics2D pg_src;
PGraphics2D pg_dst;

boolean pause = false;

public void settings() {
  fullScreen(P2D);
  PJOGL.setIcon(dataPath("icon/icon.png"));
  smooth(0);
}

/* Initialization of the application */

/*
 setup()
 Method for setup of the sketch
 */
public void setup() {
  surface.setTitle(title);
  frameRate(1000);

  setupTextures();
  setupGUI();
}

public void setupTextures() {
  /* Initialization of textures */
  pg_src = createTexture();
  pg_dst = createTexture();

  /* Initialization for the first circle */
  addSeed();
}

/*
 createTexture()
 Method for initializing textures
 */
public PGraphics2D createTexture() {
  PGraphics2D pg = (PGraphics2D) createGraphics(sourceWidth, sourceHeight, P2D);
  pg.beginDraw();
  pg.textureSampling(2);
  pg.blendMode(REPLACE);
  pg.clear();
  pg.noStroke();
  pg.endDraw();
  return pg;
}

public void setupGUI() {
  gui = new LazyGui(this, new LazyGuiSettings()
    .setSketchNameOverride("MENU")

  /* Autoload settings */
    .setLoadLatestSaveOnStartup(true)

  /* Autosave settings */
    .setAutosaveOnExit(true)
    .setAutosaveLockGuardEnabled(true)
    .setAutosaveLockGuardMillisLimit(1000)

  /* Mouse settings */
    .setMouseHideWhenDragging(true)
    .setMouseConfineToWindow(false)

  /* Layout settings */
    .setAutosuggestWindowWidth(true)
    .setCellSize(27)
    .setMainFontSize(20)
    .setSideFontSize(18)
    .setStartGuiHidden(false)
    .setThemePreset("dark"));

  gui.hide("options");
  gui.hide("saves/autosave rules");
}

public void addSeed() {
  pg_src.beginDraw();
  pg_src.background(0xFFFF0000);
  pg_src.fill(0x0000FFFF);
  pg_src.circle(sourceWidth / 2, sourceHeight / 2, brushSize);
  pg_src.endDraw();
}

/* Frame drawing of the application */

/*
 draw()
 Method for the whole reaction diffusion algorithm
 */
public void draw() {

  /* Reloading shaders */
  shader_grayscott = ShaderReloader.getShader(computeShader);
  shader_render = ShaderReloader.getShader(renderShader);

  /* Multipass rendering of the algorithm */
  for (int i = 0; i < 20; i++) {
    if (!pause) {
      brush();
      calculate();
    }
  }

  /* Rendering result */
  render();

  /* Creating GUI components */
  guiControls();
}

/*
 brush()
 Method for working with brush
 */
public void brush() {
  if (mousePressed && gui.isMouseOutsideGui() && (mouseButton == LEFT || mouseButton == RIGHT)) {
    pg_src.resetShader();
    pg_src.beginDraw();

    if (mouseButton == LEFT) {
      pg_src.fill(0x0000FFFF);
    } else if (mouseButton == RIGHT) {
      pg_src.fill(0xFFFF0000);
    }

    pg_src.circle(mouseX * (float(sourceWidth) / width), mouseY * (float(sourceHeight) / height), brushSize);

    pg_src.endDraw();
  }
}

/*
 pass()
 Method for one pass of the reaction diffusion algorithm
 */
public void calculate() {
  /* Setting the values for the Gray-Scott algorithm in the shader */
  shader_grayscott.set("dA", dA);
  shader_grayscott.set("dB", dB);
  shader_grayscott.set("feed", feed);
  shader_grayscott.set("kill", kill);
  shader_grayscott.set("dt", dT);
  shader_grayscott.set("wh_rcp", 1.0 / sourceWidth, 1.0 / sourceHeight);
  shader_grayscott.set("tex", pg_src);

  /* One pass of the algorithm */
  pg_dst.beginDraw();
  pg_dst.shader(shader_grayscott);
  pg_dst.rectMode(CORNER);
  pg_dst.rect(0, 0, sourceWidth, sourceHeight);
  pg_dst.endDraw();

  swap();
}

/*
 swap()
 Method for swapping textures
 */
public void swap() {
  PGraphics2D pg_tmp = pg_src;
  pg_src = pg_dst;
  pg_dst = pg_tmp;
}

public void render() {
  shader_render.set("wh_rcp", 1.0 / width, 1.0 / height);
  shader_render.set("tex", pg_src);
  shader_render.set("targetColor", red(targetColor), green(targetColor), blue(targetColor));
  shader_render.set("secondaryColor", red(secondaryColor), green(secondaryColor), blue(secondaryColor));
  shader_render.set("backgroundColor", red(backgroundColor), green(backgroundColor), blue(backgroundColor));

  ShaderReloader.filter(renderShader);
}

/* GUI of the application */

/*
 guiControls()
 Method for setting up all the GUI elements
 */
public void guiControls() {
  addGuiAboutSection();
  addGuiHelpSection();
  addGuiParametersSection();
  addGuiColorSection();
  addGuiBrushSection();
  addGuiSaveButton();
  addGuiClearButton();
  addGuiRegrowButton();
  addGuiResetButton();
  addGuiExitButton();
}

/*
 addGuiAboutSection()
 Method for creating About section of the GUI
 */
public void addGuiAboutSection() {
  gui.pushFolder("about");
  gui.textSet("author", "Hana Tokarova\n484176");
  gui.textSet("info", "this application was\ncreated as a part of\nPV097 course in the\nsemester spring 2023");
  gui.textSet("algorithm", "reaction diffusion\nsimulates A and B\nchemicals reacting\nand diffusing on a\ngrid using the Gray-\nScott model");
  gui.popFolder();
}

/*
 addGuiHelpSection()
 Method for creating Help section of the GUI
 */
public void addGuiHelpSection() {
  gui.pushFolder("help");
  gui.textSet("gui", "gui can be placed\nanywhere by dragging\nindividual windows");
  gui.textSet("mouse", "hold right-click to\ndraw on canvas,\nleft-click to erase");
  gui.textSet("keyboard", "use 'h' key to disable\n or enable gui, use\n'spacebar' to pause\nor unpause canvas");
  gui.textSet("saves", "load or export\nyour favourite\ncombinations");
  gui.textSet("parameters", "change presets by\nclicking, feed and\nkill parameters by\ndragging a mouse");
  gui.textSet("colors", "change colors by\ndragging a mouse");
  gui.textSet("brush", "change brush size\n by dragging a mouse");
  gui.popFolder();
}

/*
 addGuiParametersSection()
 Method for creating Parameters section of the GUI
 */
public void addGuiParametersSection() {
  /* Parameter settings button*/
  gui.pushFolder("parameters");

  String mode = gui.radio("preset", new String[]{"solitons", "spiral waves", "chaos", "stripes", "mitosis", "worms and loops", "turing", "super resonant", "custom"}, "solitons");

  float newFeed = gui.slider("feed", feed, 0, 1);
  if (abs(feed - newFeed) > 0.01) {
    gui.radioSet("preset", "custom");
    feed = newFeed;
  }

  float newKill = gui.slider("kill", kill, 0, 1);
  if (abs(kill - newKill) > 0.01) {
    gui.radioSet("preset", "custom");
    kill = newKill;
  }

  choosePresetValues(mode);

  if (!mode.equals(selectedMode)) {
    gui.sliderSet("feed", feed);
    gui.sliderSet("kill", kill);
    selectedMode = mode;
  }

  gui.popFolder();
}

/*
 choosePresetValues()
 Helper method for choosing the appropriate value from the radio buttons of presets
 */
public void choosePresetValues(String mode) {
  if (mode.equals("spiral waves")) {
    feed = 0.0118f;
    kill = 0.0475f;
  } else if (mode.equals("solitons")) {
    feed = 0.055;
    kill = 0.062;
  } else if (mode.equals("chaos")) {
    feed = 0.026;
    kill = 0.051;
  } else if (mode.equals("stripes")) {
    feed = 0.03;
    kill = 0.059;
  } else if (mode.equals("mitosis")) {
    feed = 0.02035;
    kill = 0.05546;
  } else if (mode.equals("worms and loops")) {
    feed = 0.0820;
    kill = 0.0600;
  } else if (mode.equals("turing")) {
    feed = 0.0420;
    kill = 0.0590;
  } else if (mode.equals("super resonant")) {
    feed = 0.0300;
    kill = 0.0565;
  }
}

/*
 addGuiColorSection()
 Method for creating Color section of the GUI
 */
public void addGuiColorSection() {
  gui.pushFolder("colors");

  gui.colorPicker("color #1", hue(targetColor) / 255.0, saturation(targetColor) / 255.0, brightness(targetColor) / 255.0, 1);
  PickerColor newTargetColor = gui.colorPicker("color #1");
  targetColor = color(newTargetColor.hex);
  gui.hide("color #1/alpha");

  gui.colorPicker("color #2", hue(secondaryColor) / 255.0, saturation(secondaryColor) / 255.0, brightness(secondaryColor) / 255.0, 1);
  PickerColor newSecondaryColor = gui.colorPicker("color #2");
  secondaryColor = color(newSecondaryColor.hex);
  gui.hide("color #2/alpha");

  gui.colorPicker("background", hue(backgroundColor) / 255.0, saturation(backgroundColor) / 255.0, brightness(backgroundColor) / 255.0, 1);
  PickerColor newBackgroundColor = gui.colorPicker("background");
  backgroundColor = color(newBackgroundColor.hex);
  gui.hide("background/alpha");

  gui.popFolder();
}

/*
 addGuiBrushSection()
 Method for creating Brush section of the GUI
 */
public void addGuiBrushSection() {
  gui.pushFolder("brush");
  float x = gui.slider("brush size", brushSize, 50, 1000);
  brushSize = x;
  gui.popFolder();
}

/*
 addGuiSaveButton()
 Method for creating Save button in the GUI
 */
public void addGuiSaveButton() {
  if (gui.button("save image")) {
    PGraphics2D pg_out = (PGraphics2D) createGraphics(pg_src.width, pg_src.height, P2D);
    pg_out.beginDraw();
    pg_out.copy(pg_src, 0, 0, pg_src.width, pg_src.height, 0, 0, pg_src.width, pg_src.height);
    pg_out.endDraw();
    shader_render.set("wh_rcp", 1.0 / sourceWidth, 1.0 / sourceHeight);
    pg_out.filter(shader_render);
    pg_out.save("/results/ReactionDiffusionShowcase-" + year() + month() + day() + "-" + hour() + minute() + second() + ".jpg");
    shader_render.set("wh_rcp", 1.0 / width, 1.0 / height);
  }
}

/*
 addGuiClearButton()
 Method for creating Clear button in the GUI
 */
public void addGuiClearButton() {
  if (gui.button("clear")) {
    pg_src.resetShader();

    pg_src.beginDraw();
    pg_src.background(0xFFFF0000);
    pg_src.endDraw();
  }
}

/*
 addGuiRegrowButton()
 Method for creating Regrow button in the GUI
 This button draws the initial circle in the middle of the screen
 */
public void addGuiRegrowButton() {
  if (gui.button("regrow")) {
    pg_src.resetShader();

    addSeed();
  }
}

/*
 addGuiResetButton()
 Method for creating Reset button in the GUI
 */
public void addGuiResetButton() {
  if (gui.button("reset")) {
    pg_src.resetShader();
    gui.colorPickerSet("colors/background", #FFFFFF);
    gui.colorPickerSet("colors/color #1", #32EE90);
    gui.colorPickerSet("colors/color #2", #0000FF);
    gui.radioSet("parameters/preset", "solitons");
  }
}

/*
 addGuiExitButton()
 Method for creating Exit button in the GUI
 */
public void addGuiExitButton() {
  if (gui.button("exit")) {
    exit();
  }
}

/*
 keyPressed()
 Method for getting key presses
 */
void keyPressed() {
  switch(key) {
  case ' ':
    pause = !pause;
    break;
  }
}
